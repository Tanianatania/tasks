﻿using RabbitMQ.Client;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;

namespace SharedQueueService.QueueServices
{
    public class MessageConsumerScopeFactory: IMessageConsumerScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IMessageConsumerScope Open(MessageScopeSetting messageScopeSetting)
        {
            return new MessageConsumerScope(_connectionFactory, messageScopeSetting);
        }

        public IMessageConsumerScope Connect (MessageScopeSetting messageScopeSetting)
        {
            var mqConsumerScope = Open(messageScopeSetting);
            mqConsumerScope.MessageConsumer.Connect();
            return mqConsumerScope;

        }
    }
}
