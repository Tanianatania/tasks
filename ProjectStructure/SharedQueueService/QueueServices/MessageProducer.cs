﻿using RabbitMQ.Client;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.QueueServices
{
    public class MessageProducer: IMessageProducer
    {
        private readonly IBasicProperties _properties;
        private readonly MessageProducerSetting _messageProduceSetting;

        public MessageProducer(MessageProducerSetting messageProducerSetting)
        {
            _messageProduceSetting = messageProducerSetting;
            _properties = messageProducerSetting.Channel.CreateBasicProperties();
            _properties.Persistent = true;
        }
        public void Send(string message,string type=null)
        {
            if(!string.IsNullOrEmpty(type))
            {
                _properties.Type=type;
            }
            var body = Encoding.UTF8.GetBytes(message);
            _messageProduceSetting.Channel.BasicPublish(_messageProduceSetting.PublicationAddress, _properties, body);
        }

        public void SendTyted(Type type,string message)
        {
            Send(message, type.AssemblyQualifiedName);
        }
    }
}
