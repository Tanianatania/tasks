﻿using System;
using System.Collections.Generic;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
   public class ProjectRepository : IRepository<Project>
    {
        private readonly ApplicationDbContext _context;

        public ProjectRepository(ApplicationDbContext context)
        {
            _context = context;
            _disposed = false;
        }

        public async Task<List<Project>> GetList()
        {
            return await _context.Projects
                .Include(b=>b.Team)
                .Include(b=>b.Author)
                .ToListAsync() ;
        }

        public async Task<Project> Get(int id)
        {
            return await _context.Projects
                .Include(b => b.Team)
                .Include(b => b.Author)
                .FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task Create(Project item)
        {
            await _context.Projects.AddAsync(item);
        }

        public async Task Update(Project item)
        {
            var oldProject = await _context.Projects.FirstOrDefaultAsync(a => a.Id == item.Id);
            if (oldProject != null)
            {
                var old= _context.Projects.Update(item);
            }
        }

        public async Task Delete(int id)
        {
            var oldProject = await _context.Projects.FirstOrDefaultAsync(a => a.Id == id);
            if (oldProject != null)
            {
                _context.Projects.Remove(oldProject);
            }
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        #region Dispose pattern

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
