﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class TaskRepository : IRepository<Tasks>
    {

        private readonly ApplicationDbContext _context;

        public TaskRepository(ApplicationDbContext context)
        {
            _context = context;
            _disposed = false;
        }

        public async Task<List<Tasks>> GetList()
        {
            return await _context.Tasks
                .Include(p=>p.Performer)
                .Include(i=>i.Project)
                .ToListAsync();
        }

        public async Task<Tasks> Get(int id)
        {
            return await _context.Tasks
                .Include(p => p.Performer)
                .Include(i => i.Project)
                .FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task Create(Tasks item)
        {
            await _context.Tasks.AddAsync(item);
        }

        public async Task Update(Tasks item)
        {
            var oldTasks = await _context.Tasks.FirstOrDefaultAsync(a => a.Id == item.Id);
            if (oldTasks != null && !oldTasks.Equals(item))
            {
                _context.Tasks.Update(item);
            }
        }

        public async Task Delete(int id)
        {
            var oldTasks = await _context.Tasks.FirstOrDefaultAsync(a => a.Id == id);
            if (oldTasks != null)
            {
                _context.Tasks.Remove(oldTasks);
            }
        }

        public async Task Save()
        {
           await _context.SaveChangesAsync();
        }

        #region Dispose pattern

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}

