﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly ApplicationDbContext _context;

        public TeamRepository(ApplicationDbContext context)
        {
            _context = context;
            _disposed = false;
        }

        public Task<List<Team>> GetList()
        {
            return _context.Teams.ToListAsync();
        }

        public Task<Team> Get(int id)
        {
            return _context.Teams.FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task Create(Team item)
        {
            await _context.Teams.AddAsync(item);
        }

        public async Task Update(Team item)
        {
            var oldTasks = await _context.Teams.FirstOrDefaultAsync(a => a.Id == item.Id);
            if (oldTasks != null)
            {
                _context.Teams.Update(item);
            }
        }

        public async Task Delete(int id)
        {
            var task = await _context.Teams.FirstOrDefaultAsync(a => a.Id == id);
            if (task != null)
            {
                _context.Teams.Remove(task);
            }
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        #region Dispose pattern

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        #endregion
    }
}
