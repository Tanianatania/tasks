﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class User
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(15)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(20)]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public virtual Team Team { get; set; }

        public override string ToString()
        {
            return $" First name: {FirstName}, Last name: {LastName}, Email: {Email}, Birthday: {Birthday}," +
                $"Registered at: {RegisteredAt}, Team id: {TeamId} ";
        }
    }
}
