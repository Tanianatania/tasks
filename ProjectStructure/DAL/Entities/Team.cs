﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
   public class Team
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(15)]
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
