﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IService<TeamDTO> _service;

        public TeamsController(IService<TeamDTO> service)
        {
            _service = service;
        }

        // GET: api/Teams
        [HttpGet]
        public async Task<IEnumerable<TeamDTO>> Get()
        {
            return await _service.GetList();
        }

        // GET: api/Teams/5
        [HttpGet("{id}", Name = "GetTeam")]
        public async Task<TeamDTO> Get(int id)
        {
            return await _service.Get(id);
        }

        // POST: api/Teams
        [HttpPost]
        public async Task Post([FromBody] string value)
        {
            TeamDTO item = JsonConvert.DeserializeObject<TeamDTO>(value);
            await _service.Create(item);
        }

        // PUT: api/Teams
        [HttpPut]
        public async Task Put([FromBody] string value)
        {
            TeamDTO item = JsonConvert.DeserializeObject<TeamDTO>(value);
            await _service.Update(item);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _service.Delete(id);
        }
    }
}
