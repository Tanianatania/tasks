﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IService<UserDTO> _service;

        public UsersController(IService<UserDTO> service)
        {
            _service = service;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<IEnumerable<UserDTO>> Get()
        {
            return await _service.GetList();
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetUser")]
        public async Task<UserDTO> Get(int id)
        {
            return await _service.Get(id);
        }

        // POST: api/Users
        [HttpPost]
        public async Task Post([FromBody] string value)
        {
            UserDTO item = JsonConvert.DeserializeObject<UserDTO>(value);
            await _service.Create(item);
        }

        // PUT: api/Users/5
        [HttpPut]
        public async Task Put( [FromBody] string value)
        {
            UserDTO item = JsonConvert.DeserializeObject<UserDTO>(value);
            await _service.Update(item);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _service.Delete(id);
        }
    }
}
