﻿using System;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using BLL.Services;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SharedQueueService.Interfaces;
using ConnectionFactory = SharedQueueService.QueueServices.ConnectionFactory;
using SharedQueueService.QueueServices;
using RabbitMQ.Client;
using BLL.Hubs;
using DAL;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructure
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        [Obsolete]
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IRepository<Project>, ProjectRepository>();
            services.AddScoped<IRepository<User>, UserRepository>();
            services.AddScoped<IRepository<Team>, TeamRepository>();
            services.AddScoped<IRepository<TaskStateModel>, TaskStateModelRepository> ();
            services.AddScoped<IRepository<Tasks>, TaskRepository>();          

            services.AddScoped<IService<ProjectDTO>, ProjectService>();
            services.AddScoped<ILinqService, LinqService>();
            services.AddScoped<IService<UserDTO>, UserService>();
            services.AddScoped<IService<TeamDTO>, TeamService>();
            services.AddScoped<IService<TaskStateModelDTO>, TaskStateModelService>();
            services.AddScoped<ITaskService, TaskService>();


            services.AddScoped<IMessageQueue, MessageQueue>();
            services.AddSingleton<IConnectionFactory>(x => new ConnectionFactory(new Uri(Configuration.GetSection("Rabbit").Value)));
           
            services.AddScoped<IMessageProducer, MessageProducer>();
            services.AddScoped<IMessageProducerScope, MessageProducerScope>();
            services.AddSingleton<IMessageProducerScopeFactory, MessageProducerScopeFactory>();

            services.AddScoped<IMessageConsumer, MessageConsumer>();
            services.AddScoped<IMessageConsumerScope, MessageConsumerScope>();
            services.AddSingleton<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>();

            services.AddAutoMapper();
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(builder => builder
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
            .WithOrigins("https://localhost:44354"));

            app.UseHttpsRedirection();
            app.UseSignalR(routes =>
            {
                routes.MapHub<ProjectHub>("/projectHub");
            });
           app.UseMvc();
        }
    }
}
