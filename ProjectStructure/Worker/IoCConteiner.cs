﻿using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using SharedQueueService.Interfaces;
using SharedQueueService.QueueServices;
using System;
using System.Configuration;

namespace Worker
{
    public class IoCConteiner
    {
        public IoCConteiner(IServiceCollection services)
        {
            services.AddTransient<MessageService>();
            services.AddScoped<IMessageQueue, MessageQueue>();
            services.AddScoped<MessageService>();
            services.AddSingleton<IConnectionFactory>(x => new SharedQueueService.QueueServices.ConnectionFactory(new Uri("amqp://guest:guest@localhost:5672")));
            services.AddScoped<IMessageProducer, MessageProducer>();
            services.AddScoped<IMessageProducerScope, MessageProducerScope>();
            services.AddSingleton<IMessageProducerScopeFactory, MessageProducerScopeFactory>();

            services.AddScoped<IMessageConsumer, MessageConsumer>();
            services.AddScoped<IMessageConsumerScope, MessageConsumerScope>();
            services.AddSingleton<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>();

            var provider = services.BuildServiceProvider();

            using (var service1 = provider.GetService<MessageService>())
            {
                Console.ReadKey();
            }
        }
    }
}
