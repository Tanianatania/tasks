﻿using AutoMapper;
using BLL.DTO;
using BLL.DTO.LinqModels;
using BLL.Hubs;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class LinqService : ILinqService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Tasks> _taskRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Team> _teamRepository;
        IMessageProducerScope _messageProducerScope;
        IMessageConsumerScope _messageConsumerScope;
        private readonly IHubContext<ProjectHub> _projectHub;
        private List<TasksDTO> _tasks;
        private List<ProjectDTO> _projects;
        private List<UserDTO> _users;
        private List<TeamDTO> _teams;

        public  LinqService(IRepository<Project> projectRepository,
            IRepository<Tasks> taskRepository,
            IRepository<User> userRepository,
            IRepository<Team> teamRepository,
        IMessageProducerScopeFactory messageProducerScopeFactory,
             IMessageConsumerScopeFactory messageConsumerScopeFactory,
             IHubContext<ProjectHub> projectHub,
            IMapper mapper)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSetting
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSetting
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
            _messageConsumerScope.MessageConsumer.Received += GetValue;

            _projectHub = projectHub;
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _userRepository = userRepository;
            _teamRepository = teamRepository;
            _mapper = mapper;
        }

        private void GetValue(object sender, BasicDeliverEventArgs arg)
        {
            var value = Encoding.UTF8.GetString(arg.Body);
            _projectHub.Clients.All.SendAsync("GetNotification", value);
            _messageConsumerScope.MessageConsumer.SetActnowledge(arg.DeliveryTag, true);
        }

        public async Task<IEnumerable<ResultModelDTO>> GetAllFromFile()
        {
            List<ResultModelDTO> result = new List<ResultModelDTO>();
            using (StreamReader sr = new StreamReader("../Worker/Result.txt"))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    result.Add(JsonConvert.DeserializeObject<ResultModelDTO>(line));
                }
            }
            return result;
        }
        public async Task<IEnumerable<CountOfTasksInProjectsDTO>> GetCountOfTaskInProjectByUserId(int Id)
        {
            _tasks = (await _taskRepository.GetList()).Select(item => _mapper.Map<Tasks, TasksDTO>(item)).ToList();
            _projects = (await _projectRepository.GetList()).Select(item => _mapper.Map<Project, ProjectDTO>(item)).ToList();
            _messageProducerScope.MessageProducer.Send("Get Count Of Task in project by user Id");
            var result = from project in _projects
                         join task in _tasks on project.Id equals task.ProjectId
                         where task.PerformerId == Id
                         group task by project into ress
                         select new CountOfTasksInProjectsDTO { Project = ress.Key, CountOfTasks = ress.Count() };
            return result;
        }
        public async Task<ProjectInformationDTO> GetByProjectId(int Id)
        {
            _projects = (await _projectRepository.GetList()).Select(item => _mapper.Map<Project, ProjectDTO>(item)).ToList();
            _users = (await _userRepository.GetList()).Select(item => _mapper.Map<User, UserDTO>(item)).ToList();
            _messageProducerScope.MessageProducer.Send("Get Information about project by Id");
            var result = (from project in _projects
                          join user in _users on project.TeamId equals user.TeamId
                          where project.Id == Id
                          group user by project into ress
                          select new ProjectInformationDTO
                          {
                              Project = _projects.Where(a => a.Id == Id).FirstOrDefault(),
                              TaskWithLongestDescrition = _tasks.Where(a => a.ProjectId == Id).OrderByDescending(a => a.Description.Length).FirstOrDefault(),
                              TaskWithShortestName = _tasks.Where(a => a.ProjectId == Id).OrderBy(a => a.Name.Length).FirstOrDefault(),
                              CountOfUser = ress.Count()
                          }).FirstOrDefault();
            return result;
        }
        public async Task<UserInformationDTO> GetByUserId(int Id)
        {
            _users = (await _userRepository.GetList()).Select(item => _mapper.Map<User, UserDTO>(item)).ToList();
            _messageProducerScope.MessageProducer.Send("Get Information about user by Id");
            var result = (from user in _users
                          where user.Id == Id
                          select new UserInformationDTO
                          {
                              User = user,
                              MinProject = _projects.Where(a => a.AuthorId == Id).OrderByDescending(a => a.CreatedAt).First(),
                              CountOfTaskInMinProject = _tasks.Where(a => a.ProjectId == (_projects.Where(b => b.AuthorId == Id).OrderByDescending(b => b.CreatedAt).First().Id)).Count(),
                              CountOfCanceledOrNotFinishedTasks = _tasks.Where(a => a.PerformerId == Id && (a.State == 2 || a.State == 4)).Count(),
                              LongestTask = _tasks.Where(a => a.PerformerId == Id).OrderByDescending(a => (a.FinishedAt - a.CreatedAt)).First()
                          }).FirstOrDefault();
            return result;
        }
        public async Task<IEnumerable<FinishedTasksDTO>> GetFinishedTasks(int Id)
        {
            _tasks = (await _taskRepository.GetList()).Select(item => _mapper.Map<Tasks, TasksDTO>(item)).ToList();
            _messageProducerScope.MessageProducer.Send("Get finished tasks");
            var result = _tasks.Where(a => a.PerformerId == Id && a.FinishedAt.Year == 2019 && a.State == 3).Select(a => new FinishedTasksDTO { Id = a.Id, Name = a.Name }).ToList();
            return result;
        }
        public async Task<IEnumerable<UserWithTasksDTO>> GetSortedListByUserAndTeam()
        {
            _users = (await _userRepository.GetList()).Select(item => _mapper.Map<User, UserDTO>(item)).ToList();
            _tasks = (await _taskRepository.GetList()).Select(item => _mapper.Map<Tasks, TasksDTO>(item)).ToList();
            _messageProducerScope.MessageProducer.Send("Get sorted list with user and team");
            var result = from user in _users
                         join task in _tasks on user.Id equals task.PerformerId
                         orderby user.FirstName
                         group task by user into ress
                         select new UserWithTasksDTO { User = ress.Key, Tasks = ress.OrderByDescending(a => a.Name.Length).ToList() };
            return result;
        }
        public async Task<IEnumerable<TasksDTO>> GetTaskOfUser(int Id)
        {
            _users = (await _userRepository.GetList()).Select(item => _mapper.Map<User, UserDTO>(item)).ToList();
            _messageProducerScope.MessageProducer.Send("Get tasks of user");
            var result = _tasks.Where(a => a.PerformerId == Id && a.Name.Length < 45).ToList();
            return result;
        }
        public async Task<IEnumerable<TeamWithUserDTO> > GetTeamWithYoungUser()
        {
            _users = (await _userRepository.GetList()).Select(item => _mapper.Map<User, UserDTO>(item)).ToList();
            _teams = (await _teamRepository.GetList()).Select(item => _mapper.Map<Team, TeamDTO>(item)).ToList();
            _messageProducerScope.MessageProducer.Send("Get team with young user");
            var result = (from team in _teams
                          join user in _users on team.Id equals user.TeamId
                          orderby user.RegisteredAt
                          group user by team into ress
                          where ress.All(a => DateTime.Now.Year - a.Birthday.Year >= 12)
                          select new TeamWithUserDTO
                          {
                              Id = ress.Key.Id,
                              Name = ress.Key.Name,
                              Users = ress.ToList()
                          });
            return result;
        }
    }
}
