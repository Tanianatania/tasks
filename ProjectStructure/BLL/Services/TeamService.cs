﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using System.Linq;
using SharedQueueService.Interfaces;
using Microsoft.AspNetCore.SignalR;
using BLL.Hubs;
using RabbitMQ.Client;
using SharedQueueService.Models;
using RabbitMQ.Client.Events;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TeamService : IService<TeamDTO>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Team> _repository;
        IMessageProducerScope _messageProducerScope;
        IMessageConsumerScope _messageConsumerScope;
        private readonly IHubContext<ProjectHub> _projectHub;

        public TeamService(IRepository<Team> repository,
            IMessageProducerScopeFactory messageProducerScopeFactory,
             IMessageConsumerScopeFactory messageConsumerScopeFactory,
             IHubContext<ProjectHub> projectHub,
            IMapper mapper)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSetting
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSetting
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
            _messageConsumerScope.MessageConsumer.Received += GetValue;

            _projectHub = projectHub;
            _repository = repository;
            _mapper = mapper;
        }

        private void GetValue(object sender, BasicDeliverEventArgs arg)
        {
            var value = Encoding.UTF8.GetString(arg.Body);
            _projectHub.Clients.All.SendAsync("GetNotification", value);
            _messageConsumerScope.MessageConsumer.SetActnowledge(arg.DeliveryTag, true);
        }

        public async Task Create(TeamDTO item)
        {
            _messageProducerScope.MessageProducer.Send("Team creating was triggered");
            var team = _mapper.Map<TeamDTO, Team>(item);
            await _repository.Create(team);
            await _repository.Save();
        }

        public async Task Delete(int id)
        {
            _messageProducerScope.MessageProducer.Send("Team deleting was triggered");
            await _repository.Delete(id);
            await _repository.Save();
        }

        public async Task<TeamDTO> Get(int id)
        {
            _messageProducerScope.MessageProducer.Send("Team getting by Id was triggered");
            return _mapper.Map<Team, TeamDTO>(await _repository.Get(id));
        }

        public async Task<IEnumerable<TeamDTO>> GetList()
        {
            _messageProducerScope.MessageProducer.Send("Loading all teams was triggered");
            return (await _repository.GetList()).Select(item => _mapper.Map<Team, TeamDTO>(item));
        }

        public async Task Update(TeamDTO item)
        {
            _messageProducerScope.MessageProducer.Send("Team updating was triggered");
            var team = _mapper.Map<TeamDTO, Team>(item);
            await _repository.Update(team);
            await _repository.Save();
        }
    }
}
