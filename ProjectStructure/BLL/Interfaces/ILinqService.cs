﻿using BLL.DTO;
using BLL.DTO.LinqModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ILinqService
    {
        Task<IEnumerable<CountOfTasksInProjectsDTO>>  GetCountOfTaskInProjectByUserId(int Id);
        Task<ProjectInformationDTO> GetByProjectId(int Id);
        Task<UserInformationDTO> GetByUserId(int Id);
        Task<IEnumerable<TasksDTO>> GetTaskOfUser(int Id);
        Task<IEnumerable<ResultModelDTO>> GetAllFromFile();
        Task<IEnumerable<FinishedTasksDTO>> GetFinishedTasks(int Id);
        Task<IEnumerable<TeamWithUserDTO>> GetTeamWithYoungUser();
        Task<IEnumerable<UserWithTasksDTO>> GetSortedListByUserAndTeam();
    }
}
