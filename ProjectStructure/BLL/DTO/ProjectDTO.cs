﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public virtual UserDTO Author { get; set; }
        public int TeamId{ get; set; }
        public virtual TeamDTO Team { get; set; }
        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, Description: {Description.Replace("\n", " ")}, Create at: {CreatedAt}, Deadline: {Deadline}, " +
                $"Author id: {AuthorId}, Team id: {TeamId}\n";
        }
    }
}
