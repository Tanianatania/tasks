﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ
{
    public class Route
    {
        public const string Global = "https://localhost:44354";
        public const string Teams = "/api/Teams/";
        public const string Users = "/api/Users/";
        public const string Projects = "/api/Projects/";
        public const string Tasks = "/api/Tasks/";
        public const string States = "/api/TaskStateModels/";
        public const string Ling = "/api/Linq/";
    }
}
