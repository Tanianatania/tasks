﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    public class Project
    {
        public int id;
        public string name;
        public string description;
        public DateTime createdat;
        public DateTime deadline;
        public int authorid;
        public int teamid;

        public override string ToString()
        {
            return $"Id: {id}, Name: {name}, Description: {description.Replace("\n", " ")}, Create at: {createdat}, Deadline: {deadline}, " +
                $"Author id: {authorid}, Team id: {teamid}\n";
        }
    }
}
