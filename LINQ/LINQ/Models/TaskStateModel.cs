﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    public class TaskStateModel
    {
        public int id;
        public string value;

        public override string ToString()
        {
            return $"Id: {id}, Name: {value}";
        }
    }

}
