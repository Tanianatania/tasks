﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LINQ
{
    public class Program
    {
        public static LinqRequests requests = new LinqRequests();
        public static ShowLinqRequest showRequests = new ShowLinqRequest();
        public static CrudMenu menu = new CrudMenu();
        public static Querie queries = new Querie();
        private static AsyncLock _asyncLock=new AsyncLock();

        static async Task MenuAsync()
        {
            int number = 0;
            Console.WriteLine("Hello)\n Enter nmber");
            Console.WriteLine("\t1- Request 1\n" +
                "\t2- Request 2\n" +
                "\t3- Request 3\n" +
                "\t4- Request 4\n" +
                "\t5- Request 5\n" +
                "\t6- Request 6\n" +
                "\t7- Request 7\n");
            number = Int32.Parse(Console.ReadLine());
            int enterNumber;
            switch (number)
            {
                case 1:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var request1 = await requests.GetCountOfTaskInProjectByUserId(enterNumber);
                    showRequests.ShowCountOfTaskInProjectByUserId(request1);
                    break;
                case 2:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var request2 = await requests.GetTaskOfUser(enterNumber);
                    showRequests.ShowTaskOfUser(request2);
                    break;
                case 3:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var request3 = await requests.GetFinishedTasks(enterNumber);
                    showRequests.ShowFinishedTasks(enterNumber, request3);
                    break;
                case 4:
                    var request4 = await requests.GetTeamWithYoungUser();
                    showRequests.ShowTeamWithYoungUser(request4);
                    break;
                case 5:
                    var request5 = await requests.GetSortedListByUserAndTeam();
                    showRequests.ShowSortedListByUserAndTeam(request5);
                    break;
                case 6:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var request6 = await requests.GetByUserId(enterNumber);
                    showRequests.ShowByUserId(request6);
                    break;
                case 7:
                    Console.WriteLine("Enter project Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var request7 = await requests.GetByProjectId(enterNumber);
                    showRequests.ShowByProjectId(request7);
                    break;
            }
        }
        static async Task CreateMenu()
        {
            using (await _asyncLock.LockAsync())
            {
                int number = 0;
                bool exit = false;
                while (exit != true)
                {
                    try
                    {
                        Console.WriteLine("Hello)\n Enter nmber");
                        Console.WriteLine("\t1- CRUD with Project\n" +
                            "\t2- CRUD with Task\n" +
                            "\t3- CRUD with Team\n" +
                            "\t4- CRUD with User\n" +
                            "\t5- CRUD with TaskState\n" +
                            "\t6- Linq Request\n" +
                            "\t7-Read from File\n" +
                            "\t8-Exit");
                        number = Int32.Parse(Console.ReadLine());
                        switch (number)
                        {
                            case 1:
                                await menu.CrudProjectMenu();
                                break;
                            case 2:
                                await menu.CrudTaskMenu();
                                break;
                            case 3:
                                await menu.CrudTeamMenu();
                                break;
                            case 4:
                                await menu.CrudUserMenu();
                                break;
                            case 5:
                                await menu.CrudTaskStateMenu();
                                break;
                            case 6:
                                await MenuAsync();
                                break;
                            case 7:
                                showRequests.ShowAllFromFile(await requests.GetAllFromFile());
                                break;
                            case 8:
                                exit = true;
                                break;
                            default:
                                break;
                        }
                    }
                    catch (ArgumentNullException)
                    {
                        Console.WriteLine("Null argumants exception");
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        Console.WriteLine("Number so long or small");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    var markedTaskId = await queries.MarkRandomTaskWithDelay(1000);
                }
            }
        }
        static async Task RunHubConnection()
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44354/projectHub")
                .Build();

            await connection.StartAsync();

            connection.On<string>("GetNotification", data =>
            {
                Console.WriteLine($"* {data} ");
            });

        }
        static async Task Main(string[] args)
        {
            await RunHubConnection();
            await CreateMenu();
        }
    }
}
